var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DIR = (function () {
        function DIR() {
            this.OriginalModTime = moment().format('MM/DD/YYYY') + 'T' + moment().format('HH:mm:ssA') + 'Z';
        }
        return DIR;
    }());
    var DirRequest = (function (_super) {
        __extends(DirRequest, _super);
        function DirRequest() {
            _super.apply(this, arguments);
        }
        DirRequest.prototype.docId = function (routeData) {
            return routeData.docId;
        };
        DirRequest.prototype.onDocIdChanged = function (docId) {
            if (docId) {
                if (docId.toLowerCase() === 'new') {
                    this.isNew = true;
                    var dirDoc = new DIR();
                    this.doc = dirDoc;
                    console.log('docID', docId);
                }
                else {
                    this.isNew = false;
                }
            }
        };
        DirRequest.prototype.onRouteChanged = function (route) {
        };
        DirRequest.prototype._onSelectedDoc = function (newVal, oldVal) {
            if (newVal) {
                if (newVal !== 'new') {
                    var ajax = this.$.docAjax;
                    ajax.generateRequest();
                }
            }
        };
        DirRequest.prototype._onDoc = function (newVal, oldVal) {
        };
        DirRequest.prototype._parseRequest = function (polymerEvent) {
        };
        DirRequest.prototype._getUrl = function (settings, docUnid) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/documents/unid/';
            var suffix = docUnid;
            return prefix + mid + suffix;
        };
        DirRequest.prototype._postUrl = function (settings) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents?form=Person';
            return prefix + suffix;
        };
        DirRequest.prototype._putUrl = function (settings, docUnid) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents/unid/' + docUnid;
            return prefix + suffix;
        };
        DirRequest.prototype._submitForm = function () {
            console.log('Submit Clicked');
            console.log('this.isNew', this.isNew);
            console.log('this.doc', this.doc);
            console.log('doc = ', this.doc);
            var requestClone = (JSON.parse(JSON.stringify(this.doc)));
            if (this.isNew) {
                var postAjax = this.$.postAjax;
                postAjax.body = requestClone;
                postAjax.generateRequest();
            }
            else {
                var putAjax = this.$.putAjax;
                putAjax.body = requestClone;
                putAjax.generateRequest();
            }
        };
        DirRequest.prototype._close = function () {
            this.fire('record-saved-event', null);
            this.set('route.path', '/dirAll');
        };
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirRequest.prototype, "routeData", void 0);
        __decorate([
            computed({ type: String })
        ], DirRequest.prototype, "docId", null);
        __decorate([
            observe('routeData.docId')
        ], DirRequest.prototype, "onDocIdChanged", null);
        __decorate([
            observe('route')
        ], DirRequest.prototype, "onRouteChanged", null);
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirRequest.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object
            })
        ], DirRequest.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true,
                observer: '_onSelectedDoc'
            })
        ], DirRequest.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onDoc'
            })
        ], DirRequest.prototype, "doc", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirRequest.prototype, "richTextContent", void 0);
        __decorate([
            property({
                type: Boolean
            })
        ], DirRequest.prototype, "isNew", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirRequest.prototype, "documentId", void 0);
        __decorate([
            listen('submit.click')
        ], DirRequest.prototype, "_submitForm", null);
        DirRequest = __decorate([
            component('dir-request')
        ], DirRequest);
        return DirRequest;
    }(NowElements.BaseView));
    CFOTDirectory.DirRequest = DirRequest;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirRequest.register();

//# sourceMappingURL=dir-request.js.map
