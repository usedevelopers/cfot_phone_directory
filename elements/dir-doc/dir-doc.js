var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DIR = (function () {
        function DIR() {
            this.OriginalModTime = moment().format('MM/DD/YYYY') + 'T' + moment().format('HH:mm:ssA') + 'Z';
        }
        return DIR;
    }());
    var DirDoc = (function (_super) {
        __extends(DirDoc, _super);
        function DirDoc() {
            _super.apply(this, arguments);
        }
        DirDoc.prototype.docId = function (routeData) {
            return routeData.docId;
        };
        DirDoc.prototype.onDocIdChanged = function (docId) {
            if (docId) {
                if (docId.toLowerCase() === 'new') {
                    this.isNew = true;
                    var dirDoc = new DIR();
                    this.doc = dirDoc;
                    console.log('docID', docId);
                }
                else {
                    this.isNew = false;
                }
            }
        };
        DirDoc.prototype.onRouteChanged = function (route) {
        };
        DirDoc.prototype._onSelectedDoc = function (newVal, oldVal) {
            if (newVal) {
                if (newVal !== 'new') {
                    var ajax = this.$.docAjax;
                    ajax.generateRequest();
                }
            }
        };
        DirDoc.prototype._onDoc = function (newVal, oldVal) {
        };
        DirDoc.prototype._parseRequest = function (polymerEvent) {
        };
        DirDoc.prototype._getUrl = function (settings, docUnid) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/documents/unid/';
            var suffix = docUnid;
            return prefix + mid + suffix;
        };
        DirDoc.prototype._postUrl = function (settings) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents?form=Person';
            return prefix + suffix;
        };
        DirDoc.prototype._putUrl = function (settings, docUnid) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents/unid/' + docUnid;
            return prefix + suffix;
        };
        DirDoc.prototype._submitForm = function () {
            console.log('Submit Clicked');
            console.log('this.isNew', this.isNew);
            console.log('this.doc', this.doc);
            console.log('doc = ', this.doc);
            var requestClone = (JSON.parse(JSON.stringify(this.doc)));
            if (this.isNew) {
                var postAjax = this.$.postAjax;
                postAjax.body = requestClone;
                postAjax.generateRequest();
            }
            else {
                var putAjax = this.$.putAjax;
                putAjax.body = requestClone;
                putAjax.generateRequest();
            }
        };
        DirDoc.prototype._close = function () {
            this.fire('record-saved-event', null);
            this.set('route.path', '/dirAll');
        };
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirDoc.prototype, "routeData", void 0);
        __decorate([
            computed({ type: String })
        ], DirDoc.prototype, "docId", null);
        __decorate([
            observe('routeData.docId')
        ], DirDoc.prototype, "onDocIdChanged", null);
        __decorate([
            observe('route')
        ], DirDoc.prototype, "onRouteChanged", null);
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirDoc.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object
            })
        ], DirDoc.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true,
                observer: '_onSelectedDoc'
            })
        ], DirDoc.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onDoc'
            })
        ], DirDoc.prototype, "doc", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirDoc.prototype, "richTextContent", void 0);
        __decorate([
            property({
                type: Boolean
            })
        ], DirDoc.prototype, "isNew", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirDoc.prototype, "documentId", void 0);
        __decorate([
            listen('submit.click')
        ], DirDoc.prototype, "_submitForm", null);
        DirDoc = __decorate([
            component('dir-doc')
        ], DirDoc);
        return DirDoc;
    }(NowElements.BaseView));
    CFOTDirectory.DirDoc = DirDoc;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirDoc.register();

//# sourceMappingURL=dir-doc.js.map
