var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DirLoc = (function (_super) {
        __extends(DirLoc, _super);
        function DirLoc() {
            _super.apply(this, arguments);
        }
        DirLoc.prototype._onRowSelected = function () {
            var grid = this.$.dirLoc;
            var selectedIdx = grid.selection.selected();
            if (selectedIdx && selectedIdx.length > 0) {
                var that_1 = this;
                grid.getItem(selectedIdx, function (err, item) {
                    that_1.set('route.path', '/docLoc/' + item['@unid']);
                });
            }
        };
        DirLoc.prototype._getUrl = function (settings) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/collections/unid/5804B0DA103C745E85256CB8000294E8';
            return prefix + mid;
        };
        DirLoc.prototype._onSettings = function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                var ajax = this.$.viewAjax;
                var grid = this.$.dirLoc;
                grid.size = 50;
                grid.visibleRows = 12;
                grid.items = function (params, callback) {
                    _this._gridItemsCallback = callback;
                    _this._fetchRows(_this.settings, params.index, params.count);
                };
                grid.cellClassGenerator = this._gridCellClassGenerator;
            }
        };
        DirLoc.prototype._fetchRows = function (settings, start, count) {
            var ajax = this.$.viewAjax;
            ajax.params = {
                start: start,
                count: count
            };
            ajax.generateRequest();
        };
        DirLoc.prototype._onViewFetch = function (evt, detail) {
            var gridItems = evt.detail.response;
            var grid = this.$.dirLoc;
            this._gridItemsCallback(gridItems);
            if (this._gridSize === 0 && gridItems.length > 0) {
                grid.size = gridItems[0]['@siblings'];
                this._gridSize = grid.size;
            }
        };
        DirLoc.prototype._gridCellClassGenerator = function (cell) {
            if (cell.index === 2) {
                return 'titleCell';
            }
        };
        DirLoc.prototype._dateCellRenderer = function (cell) {
            var val = moment(cell.data).format('MM/DD/YYYY');
            cell.element.textContent = val;
        };
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirLoc.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onSettings'
            })
        ], DirLoc.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true
            })
        ], DirLoc.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirLoc.prototype, "route", void 0);
        __decorate([
            property({
                type: Function
            })
        ], DirLoc.prototype, "_gridItemsCallback", void 0);
        __decorate([
            property({
                type: Number,
                value: 0
            })
        ], DirLoc.prototype, "_gridSize", void 0);
        __decorate([
            listen('dirLoc.selected-items-changed')
        ], DirLoc.prototype, "_onRowSelected", null);
        DirLoc = __decorate([
            component('dir-loc')
        ], DirLoc);
        return DirLoc;
    }(NowElements.BaseView));
    CFOTDirectory.DirLoc = DirLoc;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirLoc.register();

//# sourceMappingURL=dir-loc.js.map
