var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DirAll = (function (_super) {
        __extends(DirAll, _super);
        function DirAll() {
            _super.apply(this, arguments);
        }
        DirAll.prototype._onRowSelected = function () {
            var grid = this.$.dirAll;
            var selectedIdx = grid.selection.selected();
            if (selectedIdx && selectedIdx.length > 0) {
                var that_1 = this;
                grid.getItem(selectedIdx, function (err, item) {
                    that_1.set('route.path', '/doc/' + item['@unid']);
                });
            }
        };
        DirAll.prototype.attached = function () {
            window.addEventListener('record-saved-event', this._onRecordSaved.bind(this));
        };
        DirAll.prototype._onRecordSaved = function () {
            console.log(this.is, '_onRecordSaved', arguments);
            var grid = this.$.dirAll;
        };
        DirAll.prototype._getUrl = function (settings) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/collections/unid/63E2264193505EAC85256CC90062408E';
            return prefix + mid;
        };
        DirAll.prototype._onSettings = function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                var ajax = this.$.viewAjax;
                var grid = this.$.dirAll;
                grid.size = 50;
                grid.visibleRows = 14;
                grid.items = function (params, callback) {
                    _this._gridItemsCallback = callback;
                    _this._fetchRows(_this.settings, params.index, params.count);
                };
                grid.cellClassGenerator = this._gridCellClassGenerator;
            }
        };
        DirAll.prototype._fetchRows = function (settings, start, count) {
            var ajax = this.$.viewAjax;
            ajax.params = {
                start: start,
                count: count
            };
            ajax.generateRequest();
        };
        DirAll.prototype._onViewFetch = function (evt, detail) {
            var gridItems = evt.detail.response;
            var grid = this.$.dirAll;
            this._gridItemsCallback(gridItems);
            if (this._gridSize === 0 && gridItems.length > 0) {
                grid.size = gridItems[0]['@siblings'];
                this._gridSize = grid.size;
            }
        };
        DirAll.prototype._gridCellClassGenerator = function (cell) {
            if (cell.index === 2) {
                return 'titleCell';
            }
        };
        DirAll.prototype._dateCellRenderer = function (cell) {
            var val = moment(cell.data).format('MM/DD/YYYY');
            cell.element.textContent = val;
        };
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirAll.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onSettings'
            })
        ], DirAll.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true
            })
        ], DirAll.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirAll.prototype, "route", void 0);
        __decorate([
            property({
                type: Function
            })
        ], DirAll.prototype, "_gridItemsCallback", void 0);
        __decorate([
            property({
                type: Number,
                value: 0
            })
        ], DirAll.prototype, "_gridSize", void 0);
        __decorate([
            listen('dirAll.selected-items-changed')
        ], DirAll.prototype, "_onRowSelected", null);
        DirAll = __decorate([
            component('dir-all')
        ], DirAll);
        return DirAll;
    }(NowElements.BaseView));
    CFOTDirectory.DirAll = DirAll;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirAll.register();

//# sourceMappingURL=dir-all.js.map
