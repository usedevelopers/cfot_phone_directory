var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DIRloc = (function () {
        function DIRloc() {
        }
        return DIRloc;
    }());
    var DirRequestLoc = (function (_super) {
        __extends(DirRequestLoc, _super);
        function DirRequestLoc() {
            _super.apply(this, arguments);
        }
        DirRequestLoc.prototype.docId = function (routeData) {
            return routeData.docId;
        };
        DirRequestLoc.prototype.onDocIdChanged = function (docId) {
            if (docId) {
                if (docId.toLowerCase() === 'new') {
                    this.isNew = true;
                    var dirDoc = new DIRloc();
                    this.doc = dirDoc;
                    console.log('docID', docId);
                }
                else {
                    this.isNew = false;
                }
            }
        };
        DirRequestLoc.prototype.onRouteChanged = function (route) {
        };
        DirRequestLoc.prototype._onSelectedDoc = function (newVal, oldVal) {
            if (newVal) {
                if (newVal !== 'new') {
                    var ajax = this.$.docAjax;
                    ajax.generateRequest();
                }
            }
        };
        DirRequestLoc.prototype._onDoc = function (newVal, oldVal) {
        };
        DirRequestLoc.prototype._parseRequest = function (polymerEvent) {
        };
        DirRequestLoc.prototype._getUrl = function (settings, docUnid) {
            console.log('_getUrl settings = ', settings);
            console.log('_getUrl docUnid = ', docUnid);
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/documents/unid/';
            var suffix = docUnid;
            return prefix + mid + suffix;
        };
        DirRequestLoc.prototype._postUrl = function (settings) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents?form=Locations';
            return prefix + suffix;
        };
        DirRequestLoc.prototype._putUrl = function (settings, docUnid) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents/unid/' + docUnid;
            return prefix + suffix;
        };
        DirRequestLoc.prototype._submitForm = function () {
            console.log('Submit Clicked');
            console.log('this.isNew', this.isNew);
            console.log('this.doc', this.doc);
            console.log('doc = ', this.doc);
            var requestClone = (JSON.parse(JSON.stringify(this.doc)));
            if (this.isNew) {
                var postAjax = this.$.postAjax;
                postAjax.body = requestClone;
                postAjax.generateRequest();
            }
            else {
                var putAjax = this.$.putAjax;
                putAjax.body = requestClone;
                putAjax.generateRequest();
            }
        };
        DirRequestLoc.prototype._close = function () {
            this.fire('record-saved-event', null);
            this.set('route.path', '/dirAll');
        };
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirRequestLoc.prototype, "routeData", void 0);
        __decorate([
            computed({ type: String })
        ], DirRequestLoc.prototype, "docId", null);
        __decorate([
            observe('routeData.docId')
        ], DirRequestLoc.prototype, "onDocIdChanged", null);
        __decorate([
            observe('route')
        ], DirRequestLoc.prototype, "onRouteChanged", null);
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirRequestLoc.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object
            })
        ], DirRequestLoc.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true,
                observer: '_onSelectedDoc'
            })
        ], DirRequestLoc.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onDoc'
            })
        ], DirRequestLoc.prototype, "doc", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirRequestLoc.prototype, "richTextContent", void 0);
        __decorate([
            property({
                type: Boolean
            })
        ], DirRequestLoc.prototype, "isNew", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirRequestLoc.prototype, "documentId", void 0);
        __decorate([
            listen('submit.click')
        ], DirRequestLoc.prototype, "_submitForm", null);
        DirRequestLoc = __decorate([
            component('dir-request-loc')
        ], DirRequestLoc);
        return DirRequestLoc;
    }(NowElements.BaseView));
    CFOTDirectory.DirRequestLoc = DirRequestLoc;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirRequestLoc.register();

//# sourceMappingURL=dir-request-loc.js.map
