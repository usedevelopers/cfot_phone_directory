namespace CFOTDirectory {

	// If something is declared as a module, this is a way to override that module. You're basically
	// saying I know this exists so just trust me
	declare var quotedPrintable;

class DIRloc{
		Location: string;
		Loc_Ext: string;
		Loc_Number: string;
		Loc_Other: string;
    constructor(){	
    }

}

	@component('dir-request-loc')
	export class DirRequestLoc extends NowElements.BaseView {

		@property({
			type: Object,
			notify: true
		})
		routeData: any;

		@computed({ type: String })
		docId(routeData) {
			return routeData.docId;
		}

		@observe('routeData.docId')
		onDocIdChanged(docId) {
			if (docId) {
				if (docId.toLowerCase() === 'new') {
					this.isNew = true;
					let dirDoc = new DIRloc();
					this.doc = dirDoc;
					console.log('docID', docId)
				} else {
					//console.log('Existing Document, docId', docId);
					this.isNew = false;
				}
			}
		}
        

		@observe('route')
		onRouteChanged(route) {
			//console.debug(this.is, 'route', route);
		}


		/**
		 * Boolean for controlling the spinner
		 * @type {Boolean}
		 */
		@property({
			type: Boolean,
			notify: true
		})
		mainLoading: boolean;
		/**
		 * The settings object
		 * @type {Object}
		 */
		@property({
			type: Object
		})
		settings: any;
		/**
		 * The UNID of the document to load
		 * @type {String}
		 */
		@property({
			type: String,
			notify: true,
			observer: '_onSelectedDoc'
		})
		selectedDoc: string;
		/**
		 * The document object from DDS
		 * @type {Object}
		 */
		@property({
			type: Object,
			observer: '_onDoc'
		})
		doc: any;
		/**
		 * The content of the single rich text field. You may want to make this it's own
		 * function for multiple rich text fields.
		 * @type {String}
		 */
		@property({
			type: String
		})
		richTextContent: string;
		/**
		 * Fired when the selectedDoc property changes. Generates the request for the document (doc)
		 * @param {String} newVal The new value
		 * @param {String} oldVal The old value
		 */
		private _onSelectedDoc(newVal, oldVal) {
			if (newVal) {
				if (newVal !== 'new') {
					let ajax = this.$.docAjax;
					ajax.generateRequest();
				}
			}
		}
		@property({
			type: Boolean
		})
		isNew: boolean;
		@property({
			type: String
		})
		documentId: string
		/**
		 * Fired when the doc property changes. Decodes the rich text field ('BulletinText') and populates
		 * the richTextContent property
		 * @param {Object} newVal The new value of doc
		 * @param {Object} oldVal The old value of doc
		 */
		private _onDoc(newVal, oldVal) {
			//console.log(this.is, '_onDoc', arguments);
		}


		private _parseRequest(polymerEvent: PolymerEvent) {
			// console.log('_parseRequest this.doc', this.doc);
			// if (this.doc) {
			// 	let response = polymerEvent.detail.response;
			// 	console.log('response', response);
			// 	this.doc.dateEntered = this._removeTimeZone(response.dateEntered);
			// 	this.doc.dateBUP = this._removeTimeZone(response.dateBUP);
			// 	this.doc.dateOut = this._removeTimeZone(response.dateOut);
			// 	console.log('this.doc after', this.doc);
			// }
		}


		/**
		 * Determine the URL for the document. Must wait for the settings and selectedDoc
		 * @param {Object} settings The settings object
		 * @param {String} docUnid  The docoument UNID
		 * @return {String}
		 */
		private _getUrl(settings, docUnid) {
			console.log('_getUrl settings = ', settings);
			console.log('_getUrl docUnid = ', docUnid);
			var url = null;
			var prefix = settings.NSF_URL;
			var mid = '/api/data/documents/unid/';
			var suffix = docUnid;
			return prefix + mid + suffix;
		}

		private _postUrl(settings) {
			var prefix = settings.NSF_URL;
			var suffix = '/api/data/documents?form=Locations';
			return prefix + suffix;
		}

		private _putUrl(settings, docUnid) {
			var prefix = settings.NSF_URL;
			var suffix = '/api/data/documents/unid/' + docUnid;
			return prefix + suffix;
		}

		@listen('submit.click')
		private _submitForm() {
			console.log('Submit Clicked');
			console.log('this.isNew', this.isNew);
			console.log('this.doc', this.doc);
			//this.doc.dateEntered = this._appendTimeZone(this.doc.dateEntered);
			//this.doc.dateBUP = this._appendTimeZone(this.doc.dateBUP);
			// if (this.doc.dateOut) {
			// 	this.doc.dateOut = this._appendTimeZone(this.doc.dateOut);
			// } else {
			// 	delete this.doc.dateOut;
			// }
            console.log('doc = ', this.doc);
			let requestClone = (JSON.parse(JSON.stringify(this.doc)));
			if (this.isNew) {
				let postAjax = this.$.postAjax;
				postAjax.body = requestClone;
				postAjax.generateRequest();
			} else {
				
				let putAjax = this.$.putAjax;
				putAjax.body = requestClone;
				putAjax.generateRequest();
			}
		}

		private _close() {
			this.fire('record-saved-event', null);
			this.set('route.path', '/dirAll');
		}

		// private _appendTimeZone(date: string) {
		// 	if (date && date.indexOf('T') === -1) {
		// 		return date + 'T00:00:00';
		// 	}
		// 	return date;
		// }

		// private _removeTimeZone(date: string) {
		// 	if (date && date.indexOf('T') > -1) {
		// 		var dateTime = date.split('T');
		// 		return dateTime[0];
		// 	}
		// 	return date;
		// }

	}
}

CFOTDirectory.DirRequestLoc.register();