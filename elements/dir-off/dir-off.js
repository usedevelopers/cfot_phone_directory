var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DirOff = (function (_super) {
        __extends(DirOff, _super);
        function DirOff() {
            _super.apply(this, arguments);
        }
        DirOff.prototype._onRowSelected = function () {
            var grid = this.$.dirOff;
            var selectedIdx = grid.selection.selected();
            if (selectedIdx && selectedIdx.length > 0) {
                var that_1 = this;
                grid.getItem(selectedIdx, function (err, item) {
                    that_1.set('route.path', '/doc/' + item['@unid']);
                });
            }
        };
        DirOff.prototype._getUrl = function (settings) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/collections/unid/65B0C63E06F78F5A85256CB800023B1B';
            return prefix + mid;
        };
        DirOff.prototype._onSettings = function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                var ajax = this.$.viewAjax;
                var grid = this.$.dirOff;
                grid.size = 50;
                grid.visibleRows = 12;
                grid.items = function (params, callback) {
                    _this._gridItemsCallback = callback;
                    _this._fetchRows(_this.settings, params.index, params.count);
                };
                grid.cellClassGenerator = this._gridCellClassGenerator;
            }
        };
        DirOff.prototype._fetchRows = function (settings, start, count) {
            var ajax = this.$.viewAjax;
            ajax.params = {
                start: start,
                count: count
            };
            ajax.generateRequest();
        };
        DirOff.prototype._onViewFetch = function (evt, detail) {
            var gridItems = evt.detail.response;
            var grid = this.$.dirOff;
            this._gridItemsCallback(gridItems);
            if (this._gridSize === 0 && gridItems.length > 0) {
                grid.size = gridItems[0]['@siblings'];
                this._gridSize = grid.size;
            }
        };
        DirOff.prototype._gridCellClassGenerator = function (cell) {
            if (cell.index === 2) {
                return 'titleCell';
            }
        };
        DirOff.prototype._dateCellRenderer = function (cell) {
            var val = moment(cell.data).format('MM/DD/YYYY');
            cell.element.textContent = val;
        };
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirOff.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onSettings'
            })
        ], DirOff.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true
            })
        ], DirOff.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirOff.prototype, "route", void 0);
        __decorate([
            property({
                type: Function
            })
        ], DirOff.prototype, "_gridItemsCallback", void 0);
        __decorate([
            property({
                type: Number,
                value: 0
            })
        ], DirOff.prototype, "_gridSize", void 0);
        __decorate([
            listen('dirOff.selected-items-changed')
        ], DirOff.prototype, "_onRowSelected", null);
        DirOff = __decorate([
            component('dir-off')
        ], DirOff);
        return DirOff;
    }(NowElements.BaseView));
    CFOTDirectory.DirOff = DirOff;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirOff.register();

//# sourceMappingURL=dir-off.js.map
