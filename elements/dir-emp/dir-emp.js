var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DirEmp = (function (_super) {
        __extends(DirEmp, _super);
        function DirEmp() {
            _super.apply(this, arguments);
        }
        DirEmp.prototype._onRowSelected = function () {
            var grid = this.$.dirEmp;
            var selectedIdx = grid.selection.selected();
            if (selectedIdx && selectedIdx.length > 0) {
                var that_1 = this;
                grid.getItem(selectedIdx, function (err, item) {
                    that_1.set('route.path', '/doc/' + item['@unid']);
                });
            }
        };
        DirEmp.prototype._getUrl = function (settings) {
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/collections/unid/61C2701D94A33A4E85256CB7004DDC77';
            return prefix + mid;
        };
        DirEmp.prototype._onSettings = function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                var ajax = this.$.viewAjax;
                var grid = this.$.dirEmp;
                grid.size = 50;
                grid.visibleRows = 12;
                grid.items = function (params, callback) {
                    _this._gridItemsCallback = callback;
                    _this._fetchRows(_this.settings, params.index, params.count);
                };
                grid.cellClassGenerator = this._gridCellClassGenerator;
            }
        };
        DirEmp.prototype._fetchRows = function (settings, start, count) {
            var ajax = this.$.viewAjax;
            ajax.params = {
                start: start,
                count: count
            };
            ajax.generateRequest();
        };
        DirEmp.prototype._onViewFetch = function (evt, detail) {
            var gridItems = evt.detail.response;
            var grid = this.$.dirEmp;
            this._gridItemsCallback(gridItems);
            if (this._gridSize === 0 && gridItems.length > 0) {
                grid.size = gridItems[0]['@siblings'];
                this._gridSize = grid.size;
            }
        };
        DirEmp.prototype._gridCellClassGenerator = function (cell) {
            if (cell.index === 2) {
                return 'titleCell';
            }
        };
        DirEmp.prototype._dateCellRenderer = function (cell) {
            var val = moment(cell.data).format('MM/DD/YYYY');
            cell.element.textContent = val;
        };
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirEmp.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onSettings'
            })
        ], DirEmp.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true
            })
        ], DirEmp.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirEmp.prototype, "route", void 0);
        __decorate([
            property({
                type: Function
            })
        ], DirEmp.prototype, "_gridItemsCallback", void 0);
        __decorate([
            property({
                type: Number,
                value: 0
            })
        ], DirEmp.prototype, "_gridSize", void 0);
        __decorate([
            listen('dirEmp.selected-items-changed')
        ], DirEmp.prototype, "_onRowSelected", null);
        DirEmp = __decorate([
            component('dir-emp')
        ], DirEmp);
        return DirEmp;
    }(NowElements.BaseView));
    CFOTDirectory.DirEmp = DirEmp;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirEmp.register();

//# sourceMappingURL=dir-emp.js.map
