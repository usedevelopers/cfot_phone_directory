var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DirApp = (function (_super) {
        __extends(DirApp, _super);
        function DirApp() {
            _super.apply(this, arguments);
        }
        DirApp.prototype.ready = function () {
            _super.prototype.ready.call(this);
        };
        DirApp.prototype.attached = function () {
            _super.prototype.attached.call(this);
        };
        DirApp.prototype._getSettingsUrl = function () {
            console.log(this.is, '_getSettingsUrl');
            var url = 'settings.json';
            var port = location.port;
            if (port && port !== '80' && port !== '443') {
                url = '../../settings.json';
            }
            console.log(this.is, '_getSettingsUrl, returning', url);
            return url;
        };
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirApp.prototype, "settings", void 0);
        DirApp = __decorate([
            component('dir-app')
        ], DirApp);
        return DirApp;
    }(NowElements.BasicApp));
    CFOTDirectory.DirApp = DirApp;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirApp.register();

//# sourceMappingURL=dir-app.js.map
