namespace CFOTDirectory {

	/**
	 * Sample starter application. Rename this class and namespace appropriately and add any application-specific functionality here.
	 * The base class will add a new instance of this class to the window object under the name 'app'.
	 * Refer to the base classes ({@link NowElements.BaseApp} and {@link NowElements.BasicApp}) to understand the default functionality.
	 *
	 * @author Kito D. Mann
	 */
	@component('dir-app')
	export class DirApp extends NowElements.BasicApp {

		ready() {
			super.ready();
		}
        
        
        

		attached() {
			super.attached();
		}
        
        /**
 * The settings object
 * @type {Object}
 */
@property({
    type: Object,
    notify: true
})
settings:any;
        
        /**
 * The url to the settings file
 * @type {Object}
 */
_getSettingsUrl() {
    console.log(this.is, '_getSettingsUrl');
    var url = 'settings.json';
    var port = location.port;
    // Setup for running local
    if (port && port !== '80' && port !== '443') {
        url = '../../settings.json';
    }
    console.log(this.is, '_getSettingsUrl, returning', url);
    return url;
}
        
        
	}
}

CFOTDirectory.DirApp.register();

