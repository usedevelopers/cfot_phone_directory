var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CFOTDirectory;
(function (CFOTDirectory) {
    var DIRloc = (function () {
        function DIRloc() {
        }
        return DIRloc;
    }());
    var DirDocLoc = (function (_super) {
        __extends(DirDocLoc, _super);
        function DirDocLoc() {
            _super.apply(this, arguments);
        }
        DirDocLoc.prototype.docId = function (routeData) {
            return routeData.docId;
        };
        DirDocLoc.prototype.onDocIdChanged = function (docId) {
            if (docId) {
                if (docId.toLowerCase() === 'new') {
                    this.isNew = true;
                    var dirDoc = new DIRloc();
                    this.doc = dirDoc;
                    console.log('docID', docId);
                }
                else {
                    this.isNew = false;
                }
            }
        };
        DirDocLoc.prototype.onRouteChanged = function (route) {
        };
        DirDocLoc.prototype._onSelectedDoc = function (newVal, oldVal) {
            if (newVal) {
                if (newVal !== 'new') {
                    var ajax = this.$.docAjax;
                    ajax.generateRequest();
                }
            }
        };
        DirDocLoc.prototype._onDoc = function (newVal, oldVal) {
        };
        DirDocLoc.prototype._parseRequest = function (polymerEvent) {
        };
        DirDocLoc.prototype._getUrl = function (settings, docUnid) {
            console.log('_getUrl settings = ', settings);
            console.log('_getUrl docUnid = ', docUnid);
            var url = null;
            var prefix = settings.NSF_URL;
            var mid = '/api/data/documents/unid/';
            var suffix = docUnid;
            return prefix + mid + suffix;
        };
        DirDocLoc.prototype._postUrl = function (settings) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents?form=Locations';
            return prefix + suffix;
        };
        DirDocLoc.prototype._putUrl = function (settings, docUnid) {
            var prefix = settings.NSF_URL;
            var suffix = '/api/data/documents/unid/' + docUnid;
            return prefix + suffix;
        };
        DirDocLoc.prototype._submitForm = function () {
            console.log('submitLoc Clicked');
            console.log('this.isNew', this.isNew);
            console.log('this.doc', this.doc);
            console.log('doc = ', this.doc);
            var requestClone = (JSON.parse(JSON.stringify(this.doc)));
            if (this.isNew) {
                var postAjax = this.$.postAjax;
                postAjax.body = requestClone;
                postAjax.generateRequest();
            }
            else {
                var putAjax = this.$.putAjax;
                putAjax.body = requestClone;
                putAjax.generateRequest();
            }
        };
        DirDocLoc.prototype._close = function () {
            this.fire('record-saved-event', null);
            this.set('route.path', '/dirAll');
        };
        __decorate([
            property({
                type: Object,
                notify: true
            })
        ], DirDocLoc.prototype, "routeData", void 0);
        __decorate([
            computed({ type: String })
        ], DirDocLoc.prototype, "docId", null);
        __decorate([
            observe('routeData.docId')
        ], DirDocLoc.prototype, "onDocIdChanged", null);
        __decorate([
            observe('route')
        ], DirDocLoc.prototype, "onRouteChanged", null);
        __decorate([
            property({
                type: Boolean,
                notify: true
            })
        ], DirDocLoc.prototype, "mainLoading", void 0);
        __decorate([
            property({
                type: Object
            })
        ], DirDocLoc.prototype, "settings", void 0);
        __decorate([
            property({
                type: String,
                notify: true,
                observer: '_onSelectedDoc'
            })
        ], DirDocLoc.prototype, "selectedDoc", void 0);
        __decorate([
            property({
                type: Object,
                observer: '_onDoc'
            })
        ], DirDocLoc.prototype, "doc", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirDocLoc.prototype, "richTextContent", void 0);
        __decorate([
            property({
                type: Boolean
            })
        ], DirDocLoc.prototype, "isNew", void 0);
        __decorate([
            property({
                type: String
            })
        ], DirDocLoc.prototype, "documentId", void 0);
        __decorate([
            listen('submitLoc.click')
        ], DirDocLoc.prototype, "_submitForm", null);
        DirDocLoc = __decorate([
            component('dir-doc-loc')
        ], DirDocLoc);
        return DirDocLoc;
    }(NowElements.BaseView));
    CFOTDirectory.DirDocLoc = DirDocLoc;
})(CFOTDirectory || (CFOTDirectory = {}));
CFOTDirectory.DirDocLoc.register();

//# sourceMappingURL=dir-doc-loc.js.map
