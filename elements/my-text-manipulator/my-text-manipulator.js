var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var MyApp;
(function (MyApp) {
    var TextManipulator = (function (_super) {
        __extends(TextManipulator, _super);
        function TextManipulator() {
            _super.apply(this, arguments);
        }
        TextManipulator.prototype.displayText = function (reverse, text) {
            return reverse ? this._reverseString(text) : text;
        };
        TextManipulator.prototype.counterArray = function (numberOfRepetitions) {
            var array = new Array();
            for (var i = 0; i < numberOfRepetitions; i++) {
                array.push(i);
            }
            return array;
        };
        TextManipulator.prototype.ready = function () {
            console.debug(this.is, "ready!");
        };
        TextManipulator.prototype.attached = function () {
            console.debug(this.is, "attached!");
        };
        TextManipulator.prototype._reverseString = function (s) {
            return (s === '') ? '' : this._reverseString(s.substr(1)) + s.charAt(0);
        };
        __decorate([
            property({ type: String })
        ], TextManipulator.prototype, "text", void 0);
        __decorate([
            property({ type: Boolean, value: false })
        ], TextManipulator.prototype, "reverse", void 0);
        __decorate([
            property({ type: Number, value: 1 })
        ], TextManipulator.prototype, "numberOfRepetitions", void 0);
        __decorate([
            computed({ type: String })
        ], TextManipulator.prototype, "displayText", null);
        __decorate([
            computed({ type: Array })
        ], TextManipulator.prototype, "counterArray", null);
        TextManipulator = __decorate([
            component('my-text-manipulator')
        ], TextManipulator);
        return TextManipulator;
    }(NowElements.BaseElement));
    MyApp.TextManipulator = TextManipulator;
})(MyApp || (MyApp = {}));
MyApp.TextManipulator.register();

//# sourceMappingURL=my-text-manipulator.js.map
