describe('my-text-manipulator tests', function () {
    var element;
    var originalText = "Testing is fun!";
    before(function () {
        element = fixture('my-text-manipulator');
    });
    after(function () {
    });
    beforeEach(function () {
    });
    afterEach(function () {
    });
    it('should be instantiated', function () {
        chai.expect(element.is).equals('my-text-manipulator');
    });
    it('make sure one item is displayed by default', function () {
        element.$.repeater.render();
        chai.expect(element.$.repeater.renderedItemCount).equals(1);
    });
    it('make sure the proper number of items are changed when numberOfRepetitions is set', function () {
        element.numberOfRepetitions = 5;
        element.$.repeater.render();
        chai.expect(element.$.repeater.renderedItemCount).equals(5);
    });
});

//# sourceMappingURL=my-test-manipulator-tests.js.map
