describe('my-view1 tests', function () {
    var view1;
    before(function () {
        view1 = new SimpleViewTester(fixture('my-view1'));
    });
    beforeEach(function (done) {
        view1.init(done);
    });
    afterEach(function (done) {
        view1.reset(done);
    });
    it('Should display "1" inside of the circle', function () {
        var circle = view1.circle;
        chai.expect(circle).to.not.be.null;
        chai.expect(circle.textContent).equals('1');
    });
});

//# sourceMappingURL=my-view1-tests.js.map
