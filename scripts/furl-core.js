var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Furlough;
(function (Furlough) {
    (function (StatusType) {
        StatusType[StatusType["NONE"] = 0] = "NONE";
        StatusType[StatusType["PENDING_REVIEW"] = 1] = "PENDING_REVIEW";
        StatusType[StatusType["PENDING_APPROVAL"] = 2] = "PENDING_APPROVAL";
        StatusType[StatusType["APPROVED"] = 3] = "APPROVED";
        StatusType[StatusType["DENIED"] = 4] = "DENIED";
    })(Furlough.StatusType || (Furlough.StatusType = {}));
    var StatusType = Furlough.StatusType;
    (function (SectionType) {
        SectionType[SectionType["PERSONNEL"] = 2] = "PERSONNEL";
        SectionType[SectionType["PROGRAM"] = 3] = "PROGRAM";
        SectionType[SectionType["BUSINESS_ADMINISTRATION"] = 4] = "BUSINESS_ADMINISTRATION";
        SectionType[SectionType["CHIEF_SECRETARY_OFFICE"] = 5] = "CHIEF_SECRETARY_OFFICE";
        SectionType[SectionType["WOMENS_MINISTRIES"] = 6] = "WOMENS_MINISTRIES";
        SectionType[SectionType["TERRITORIAL_COMMANDERS_OFFICE"] = 7] = "TERRITORIAL_COMMANDERS_OFFICE";
    })(Furlough.SectionType || (Furlough.SectionType = {}));
    var SectionType = Furlough.SectionType;
    var RequestHeader = (function () {
        function RequestHeader(id, name, fridaystart, fridayend, status, unit) {
            this.id = id;
            this.name = name;
            this.fridaystart = fridaystart;
            this.fridayend = fridayend;
            this.status = status;
            this.unit = unit;
        }
        return RequestHeader;
    }());
    Furlough.RequestHeader = RequestHeader;
    var THQRequestHeader = (function (_super) {
        __extends(THQRequestHeader, _super);
        function THQRequestHeader(id, name, fridaystart, fridayend, status, unit) {
            _super.call(this, id, name, fridaystart, fridayend, status, unit);
        }
        return THQRequestHeader;
    }(RequestHeader));
    Furlough.THQRequestHeader = THQRequestHeader;
    var Request = (function (_super) {
        __extends(Request, _super);
        function Request(id, name, fridaystart, fridayend, status, unit) {
            _super.call(this, id, name, fridaystart, fridayend, status, unit);
        }
        return Request;
    }(RequestHeader));
    Furlough.Request = Request;
    var THQRequest = (function (_super) {
        __extends(THQRequest, _super);
        function THQRequest(id, name, fridaystart, fridayend, status, unit) {
            _super.call(this, id, name, fridaystart, fridayend, status, unit);
        }
        return THQRequest;
    }(Request));
    Furlough.THQRequest = THQRequest;
    var User = (function () {
        function User(name) {
            this.name = name;
        }
        return User;
    }());
    Furlough.User = User;
    var BaseElement = (function (_super) {
        __extends(BaseElement, _super);
        function BaseElement() {
            _super.apply(this, arguments);
        }
        BaseElement.prototype.created = function () {
            this._settings = Furlough.settings;
        };
        return BaseElement;
    }(NowElements.BaseElement));
    Furlough.BaseElement = BaseElement;
    var BaseView = (function (_super) {
        __extends(BaseView, _super);
        function BaseView() {
            _super.apply(this, arguments);
        }
        BaseView.prototype.created = function () {
            this._settings = Furlough.settings;
        };
        return BaseView;
    }(NowElements.BaseView));
    Furlough.BaseView = BaseView;
    var Utils = (function () {
        function Utils() {
        }
        Utils.formatEnumText = function (text) {
            return text.toLowerCase().split("_").map(function (word) {
                return word[0].toUpperCase() + word.substring(1);
            }).join(" ");
        };
        Utils.unformatEnumText = function (text, defaultValue) {
            if (text) {
                return text.toUpperCase().replace(" ", "_");
            }
            return defaultValue;
        };
        Utils.getCssClassFromEnumText = function (text) {
            return text.toLowerCase().replace("_", '-');
        };
        return Utils;
    }());
    Furlough.Utils = Utils;
})(Furlough || (Furlough = {}));

//# sourceMappingURL=furl-core.js.map
