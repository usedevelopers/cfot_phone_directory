namespace Furlough {

	export enum StatusType {
		NONE, PENDING_REVIEW, PENDING_APPROVAL, APPROVED, DENIED
	}

	export enum SectionType {
		PERSONNEL = 2,
		PROGRAM = 3,
		BUSINESS_ADMINISTRATION = 4,
		CHIEF_SECRETARY_OFFICE = 5,
		WOMENS_MINISTRIES = 6,
		TERRITORIAL_COMMANDERS_OFFICE = 7
	}

	export class RequestHeader {
		constructor(public id: string,
					public name: string,
					public fridaystart: string,
					public fridayend: string,
					public status: StatusType,
					public unit: string) {
		}
	}

	export class THQRequestHeader extends RequestHeader {
		constructor(id?: string,
					name?: string,
					fridaystart?: string,
					fridayend?: string,
					status?: StatusType,
					unit?: string) {
			super(id, name, fridaystart, fridayend, status, unit);
		}
	}

	export class Request extends RequestHeader {

		command: string;
		section: SectionType;
		departmentHead: string;
		spouse: string;
		reqno: string;
		reqdate: string;
		foryear: number;
		furloughaddress: string;
		furloughresponsibilities: string;
		officersign: string;
		signedByDate: string;
		history: string;
		approvedhistory: string;
		reviewhistory: string;
		localapproverhistory: string;
		daysremaining: number;
		knownperson: string;

		constructor(id?: string,
					name?: string,
					fridaystart?: string,
					fridayend?: string,
					status?: StatusType,
					unit?: string) {
			super(id, name, fridaystart, fridayend, status, unit);
		}
	}

	export class THQRequest extends Request {

		vehicleuse: 'Yes' | 'No';
		anotherVehicleAvailable: string;
		responsibleSupervisorIsNotOfficer: 'Yes' | 'No';
		howLongKnownMarkGates: string;
		handlecash: string;
		howLongMarkGatesAssociated: string;
		understandCardUsage: 'Yes' | 'No';
		localapprrequired: 'Yes' | 'No';
		localapprovername: string;
		officersign: string;

		constructor(id?: string,
					name?: string,
					fridaystart?: string,
					fridayend?: string,
					status?: StatusType,
					unit?: string) {
			super(id, name, fridaystart, fridayend, status, unit);
		}
	}

	export class User {

		constructor(public name: string) {

		}
	}

	export abstract class BaseElement extends NowElements.BaseElement {

		_settings: Settings;

		created() {
			this._settings = Furlough.settings;
		}
	}

	export abstract class BaseView extends NowElements.BaseView {
		_settings: Settings;

		created() {
			this._settings = Furlough.settings;
		}
	}

	export class Utils {

		/**
		 * Changes all words (separated by '_') sentence places and replaces '_' with ' ')
		 * @param text text value of enum (for example: StatusType[StatusType.PENDING])
		 * @returns {string}
		 */
		static formatEnumText(text: string): string {
			return text.toLowerCase().split("_").map((word) => {
				return word[0].toUpperCase() + word.substring(1);
			}).join(" ");
		}


		static unformatEnumText(text: string, defaultValue?: string): string {
			if (text) {
				return text.toUpperCase().replace(" ", "_");
			}
			return defaultValue;
		}

		/**
		 * Changes the string to lowercase and replaceds '_' with '-'
		 * @param text text value of enum (for example: StatusType[StatusType.PENDING])
		 * @returns {string}
		 */
		static getCssClassFromEnumText(text: string): string {
			return text.toLowerCase().replace("_", '-');
		}

		// static isElementVisible(elem: HTMLElement): boolean {
		// 	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
		// }
	}
}
