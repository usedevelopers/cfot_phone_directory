namespace Furlough {

	export class Settings {

		apiUrl: string = "/api/oda";
	}

	export var settings = new Settings();
}
