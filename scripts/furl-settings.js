var Furlough;
(function (Furlough) {
    var Settings = (function () {
        function Settings() {
            this.apiUrl = "/api/oda";
        }
        return Settings;
    }());
    Furlough.Settings = Settings;
    Furlough.settings = new Settings();
})(Furlough || (Furlough = {}));

//# sourceMappingURL=furl-settings.js.map
